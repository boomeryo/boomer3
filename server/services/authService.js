const E = module.exports
const jwt = require('jsonwebtoken')
const blackListedToknes = require('../models/blackListedTokensModel')
const users = require('../models/usersModel')
require('dotenv').config()

//********************************************TOKEN VALIDATION************************************************/
//verify Token
//Foramt of token
// Authorization: bearer <access_token>
//Verify Token 
E.setTokenToReq=function(req,res,next){
    //Get Auth Header value
  const bearerHeader = req.headers['authorization'];
    //check if bearer is undefined
    if(typeof bearerHeader !== 'undefined'){
      //Split at the space
      const bearer = bearerHeader.split(' ');
      //Get token from array
      const bearerToken = bearer[1];
      //Set the token
      req.token = bearerToken;
      //Next middleware
      next();
    } else {
      //forbbiden
      res.status(403);
    }
  }  
  
  //verify jwt token
  E.verifyToken = async function (token) {
    try {
      var verified = await jwt.verify(token, process.env.TOKEN_SECRET)
      if (verified.iat && verified.exp) {
        //check if it blackListed
        let tokenIsBlackListed = await E.isTokenBlacked(token)
        if (tokenIsBlackListed ===true) {
          //token is blacklisted that means user is blocked
          console.log('token is blacklisted!')
          return false
        }
        return true
      } else {
        console.log('token is dead')
        return false
      }
    } catch (error) {
        return false
    }
}

  
//return true if user is blocked (active===false)
E.isUserBlocked = async (username) => {
  try {
    let isBlocked = await users.findUser({ username: username }, { active: 1, _id: 0 })
    if (isBlocked.active === false) {
      return true
    }
    return false
  } catch (error) {
    throw new Error(error)
  }
}

E.isTokenBlacked = async (token) => {
  let tokenIsBlocked = await blackListedToknes.findToken(token)
  if (tokenIsBlocked != null) {
    return true
  }
  return false
}

E.isUserAdmin = async (username) => {
  let userType = await users.findUser({ username: username }, { type: 1, active: 1, _id: 0 })
  console.log(userType)
  if (userType != null) {
    if (userType.type === 'admin' && userType.active === true) {
      console.log('is admin!')
      return true
    }
    console.log('not admin and not active!')
    return false
  }
  console.log('user was not found')
  return false
}

E.blackListToken = async (userID) => {
  let userToken = await users.findUser({ _id: userID }, { _id: 0, token: 1 })
  let blockToken = await blackListedToknes.insertToken(userToken.token)
  if (blockToken === true) {
    console.log('token is black listed!')
    return true
  }
  return false
}
  