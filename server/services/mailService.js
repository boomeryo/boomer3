const nodemailer = require('nodemailer');
require('dotenv').config()

//set transported with email credentails
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
           user: process.env.GMAIL_EMAIL,
           pass: process.env.GMAIL_PASS
       }
});

//Email object
const mailOptions = {
    from: process.env.GMAIL_EMAIL, // sender address
    to: ``, // list of receivers
    subject: ``, // Subject line
    html: ``// plain text body
};


//Generic function to send an email
sendEmail = (mailOptions) => {
    transporter.sendMail(mailOptions, function (info) {
        // sending succeeded
        if (info == null) {
            return console.log('Email Sent!')
        }
        console.log('Email Wasnt Sent')
    })
}

//******FUNCTIONS********/
//user registration email
module.exports.sendEmail = (c, info) => {
    var localTemplate

        //GENERIC TEMPLATE
        var generalTemplatea = `
        <html>
        <img src="https://res.cloudinary.com/dmp70q5nt/image/upload/v1537877337/sites/logowithblacksub.png" 
            style="display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
            max-width: 300px;
            height: auto;">
            <br>`

           var generalTemplateb= `<br>
            <br>
        <footer>        
            <p style="max-width: 500px; margin: auto;text-align: center;">
                &copy; 2018 Boomerb2b. All rights reserved. 
            </p>
        </footer>
        </html>`

    switch (c) {
        case 'welcomeEmail':
            localTemplate =`<p style="max-width: 500px; margin: auto;text-align: center;">Hi <b>${info.username}</b></p>
            <p style="max-width: 500px; margin: auto;text-align: center;">
            Thank you for singing up for boomer! Now all you have to do is explore deals that you will
               make the most of them. Feel free to contact us any time!
            </p>`
            //set dest
            mailOptions.to=info.username,
            mailOptions.subject="Welcome To Boomer!"
            mailOptions.html = generalTemplatea+localTemplate+generalTemplateb
            break;
        case 'postDeal':
            localTemplate =`<p style="max-width: 500px; margin: auto;text-align: center;">Hi <b>${info.username}</b></p>
            <p style="max-width: 500px; margin: auto;text-align: center;">
            Your Deal is Published!
            you can watch it By <a href="${process.env.APP_URL}/#/deals/${info.dealID}">Cliking here</a>
            </p>`
            //set dest
            mailOptions.to=info.username,
            mailOptions.subject="Your Deal Is Published!"
            mailOptions.html = generalTemplatea+localTemplate+generalTemplateb
            break
        case 'resetPassword':
            localTemplate=`<p >Hi! you are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n
            Please click on the following link, or paste this into your browser to complete the process:</p>\n\n
            <a href=${info.link}>Click here to reset your password </a>\n\n
            <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>\n`
            //set dest
            mailOptions.to=info.username,
            mailOptions.subject="Continue Password Reset Request"
            mailOptions.html = generalTemplatea+localTemplate+generalTemplateb
            break
        case 'changedPassword':
            localTemplate=`<p style="max-width: 500px; margin: auto;text-align: center;">Hi! your password changed successfully!</p>`
            //set dest
            mailOptions.to=info.username,
            mailOptions.subject="Password Change Confirmation"
            mailOptions.html = generalTemplatea+localTemplate+generalTemplateb
            break
        case 'reportEmail':
            localTemplate=`<p>We recvied a report on this deal.\n\n</p>\n\n
            <p>From: ${info.reportedBy}</p>
            <p>Reason: ${info.reason}</p>\n\n
            <p>Please handle<p>\n`
            //set dest
            mailOptions.subject = "Deal Report"
            mailOptions.to =  process.env.GMAIL_EMAIL
            mailOptions.html = generalTemplatea+localTemplate+generalTemplateb
            break;
        case 'contactMessage':
            localTemplate =
                `<p style="max-width: 500px; margin: auto;text-align: center;">
                    From:${info.name}
                    Email:${info.email}
                    Content:${info.description}
                </p>`
            mailOptions.subject = info.subject
            mailOptions.to = process.env.GMAIL_EMAIL
            mailOptions.html = generalTemplatea+localTemplate+generalTemplateb
            break;
        case 'contactMessageApproval':
            localTemplate =`<p style="max-width: 500px; margin: auto;text-align: center;">
                    This is a confirmation that we Recived your message.
                    We will do our best to help you ASAP.
                </p>`
            mailOptions.to = info.username
            mailOptions.subject = 'Thank you for your message!'
            mailOptions.html = generalTemplatea+localTemplate+generalTemplateb
            break;
    }

    //sending the email
    sendEmail(mailOptions);
};


//user posted a deal email
///user changed password email
//invatation to reset password
//user got block email
//user deal was deactivted email
//send report email




// var templates = {
//     newRegistration: `Hi, Thank you for singing up for boomer! Now all you have to do is explore deals that you will
//                       make the most of them. Feel free to contact us any time!
//                       Regrads, Boomer Team`,
//     changingPassword: `Your password was changed succesfully! You can change your password easily and anytime in order to
//                        maintain a secure account. Regrads, Boomer Team`,
//     publishDeal: `Your new dealwas published succesfully! You can watch your deal in the market and edit it in the
//                  'My Deals section. Good luck! Regrads, Boomer Team'`,
//     contactMessageAnswer: `Thank you for your message! We are on it right away and will answer you ASAP. Your feedback is 
//                            very important to us. Regrads, Boomer Team`
// }

// module.exports.sendEmailToUser=function(username,info, template){

//     switch (template) {
//         case 'newRegistration':
//             mailOptions.subject = `Welcome To Boomer!`
//             mailOptions.html = templates.newRegistration;
//             mailOptions.to = username;
//             break;
//         case 'changedPassword':
//             mailOptions.subject = 'Your Password Was Changed'
//             mailOptions.html = templates.changingPassword
//             break;
//         case 'publishDeal':
//             mailOptions.subject = `Your deal ${info}`
//             mailOptions.html = templates.publishDeal
//             mailOptions.to= username
//             break;
        
//         case 'passwordReset':
//         mailOptions.subject = `Password Resetting`
//         mailOptions.to=username
//         mailOptions.html = `<p>You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n
//         Please click on the following link, or paste this into your browser to complete the process:</p>\n\n
//         <a href=${info}>Click here to reset your password </a>\n\n
//         <p>If you did not request this, please ignore this email and your password will remain unchanged.<p>\n`
//         break;
        
//         case 'contactMessage':
//             mailOptions.subject = info.subject
//             mailOptions.to = mailOptions.from
//             mailOptions.html = `<b>from:</b> ${info.name}<p> email: ${info.email}</p>
//             <p>${info.description}</p>`
//             break;
//         case 'contactMessageSentApproval':
//             mailOptions.subject = 'Thank you for your message!'
//             mailOptions.to = username
//             mailOptions.html = templates.contactMessageAnswer
//             break;
//         default:
//             break;
//     }
//     sendEmail(mailOptions);
// }

// module.exports.sendReportEmail = (report) => { 
//     mailOptions.subject = `Report On Deal: ${report.dealID}`
//     mailOptions.to=process.env.GMAIL_EMAIL
//     mailOptions.html = 
//     `<p>We recvied a report on this deal.\n\n</p>\n\n
//     <p>From: ${report.reportedBy}</p>
//     <p>Reason: ${report.reason}</p>\n\n
//     <p>Please handle<p>\n`

//     sendEmail(mailOptions);
// }

sendEmail = (mailOptions) => {
    transporter.sendMail(mailOptions, function (info) {
        // sending succeeded
        if (info != null) {
            return console.log('email sent')
        }
        console.log(info)
    })
}
