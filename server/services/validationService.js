const E = module.exports;

//****************************VALIDATION FUNCTIONS********************************************************/
E.validateUserSignUp = (user, errors) => {
  E.validateUserName(user.username, errors);
  E.validateString(user.fullName, errors, "Full name", 25, 3);
  E.validateString(user.company, errors, "Company", 30, 2);
  E.validateString(user.country, errors, "Contry", 30, 2);
  E.validatePassword(user.password, errors);
  if (
    errors.indexOf("**password is empty") <= -1 ||
    errors.indexOf(
      "**Password must be at least 8 charecters long+1 uppercase letter +1 number+1 symbol+1 lowercase letter"
    ) <= -1
  ) {
    //if first password field is empty no need to check second one
    E.isPasswordsMatch(user.password, user.password2, errors);
  }
  // if the errors array contain errors will return false
  if (errors.length > 0) {
    return false;
  }
  return true;
};

E.validateUserLogin = (user, errors) => {
  E.validateUserName(user.username, errors);
  E.validatePassword(user.password, errors);
  if (errors.length > 0) {
    return false;
  }
  return true;
};

E.validateUserUpdateDetails = (user, errors) => {
  E.validateString(user.fullName, errors, "Full name", 25, 3);
  E.validateString(user.company, errors, "Company", 30, 2);
  E.validateString(user.country, errors, "Country", 25, 3);
  if (errors.length > 0) {
    return false;
  }
  return true;
};
E.validatePasswordUpdate = (passwords, errors) => {
  E.validatePassword(passwords.currentPassword, errors);
  E.validatePassword(passwords.newPassword1, errors);
  //if user choose his current password
  if (E.isStringsEqual(passwords.currentPassword, passwords.newPassword1)) {
    return errors.push("**you cant choose your current password");
  }
  if (E.isPasswordIsAccordingToPoliciy(passwords.newPassword1, errors)) {
    E.isPasswordsMatch(passwords.newPassword1, passwords.newPassword2, errors);
  }
  if (errors.length > 0) {
    return false;
  }
  return true;
};

E.validatePasswordReset = (passwords, errors) => {
  E.validatePassword(passwords.password1, errors);
  if (
    errors.indexOf("**password is empty") <= -1 ||
    errors.indexOf("**Password must be at least 8 charecters long+1 uppercase letter +1 number+1 symbol+1 lowercase letter") <= -1) {
    //if first password field is empty no need to check second one
    E.isPasswordsMatch(passwords.password1, passwords.password2, errors);
  }
  // if the errors array contain errors will return false
  if (errors.length > 0) {
    return false;
  }
  return true;
},
E.validateContactMessage=(message,errors)=>{
  E.validateString(message.email,errors,'Email',30,12)
  E.validateString(message.name,errors,'Name',50,3);
  E.validateString(message.subject,errors,'Subject',100,3);
  E.validateString(message.description,errors,'Description',300,20);
  if (!E.isStringIsEmail(message.email)) {
    //will return because why to check if its not an actual email
    return errors.push("**please insert a valid email address");
  }

},
  //****VALIDATE DEAL CREATION FUNCTIONS*****************/
  E.validateDealCreation = (deal, errors) => {
    E.validateString(deal.title, errors, "Title", 30, 10);
    E.validateString(deal.category, errors, "Category", 20, 3);
    E.validateString(deal.country, errors, "Country", 25, 3);
    E.validateString(deal.continent, errors, "Continent", 25, 3);
    E.validateString(deal.description, errors, "Description", 300, 20);
    E.validateDuration(deal.duration, errors);
    if (errors.length > 0) {
      return false;
    }
    return true;
  };

  E.validatePicturesType=(files, errors)=> {
    var fileSize = 5242880;
    var namesv=true
    var sizesv=true
    var typesv=true
    //check if name is with pic type
    var r=RegExp(/\.(png|jpeg|jpg|JPEG|PNG|JPG)$/)
if(!files){
    return errors.push("**You need to choose  at least one picture")
}
    for (const key in files) {
            if (files.hasOwnProperty(key)) {
            var file= files[key];
            //check if the name of the files contains image type
            if(!r.test(file.name)){
                namesv=false
            }
            //check the size of the files
            if(file.size>fileSize){
                sizesv=false;
            }
            //check the type of the files
            if(file.mimetype!='image/jpeg' && file.mimetype!='image/png' && file.mimetype!='image/jpg'){
                typesv=false;
            }
                }
    }
    if(namesv && sizesv && typesv){
        console.log('files checked everything is ok!');
        return true;
    }else{
        console.log('found errors');
        errors.push("**The Files you selected are not valid, check their size and type!")
        return false;
    }
}

E.validateDuration = (duration, errors) => {
  if (E.isStringEmpty(duration) || !E.isStringisString(duration)) {
    return errors.push("**You didnt select duration");
  }
  let eq10 = E.isStringsEqual(duration, "10");
  let eq20 = E.isStringsEqual(duration, "20");
  let eq30 = E.isStringsEqual(duration, "30");
  //if not equal to at least of of them, will insert an error
  if (
    (eq10 && !eq20 && !eq30) ||
    (!eq10 && eq20 && !eq30) ||
    (!eq10 && !eq20 && eq30)
  ) {
    return;
  } else {
    errors.push(
      "**the duration you choose is invalid please choose 10 or 20 or 30 days"
    );
  }
};

E.validatePictures = (pictures, errors) => {
  var size = 0, key;
  for (key in pictures) {
      if (pictures.hasOwnProperty(key)) size++;
  }
  console.log(size)

  if (size <= 0) {
    return errors.push("**you didnt choose any pictures! choose at least one");
  }
  if (pictures.length > 5) {
    return errors.push("**You cant choose more than 5 pictures");
  }
  E.validatePicturesType(pictures, errors);
  if (errors.length <= 0) {
    return false
  }
  return true
};

E.validateString = (str, errors, fieldName, long, short) => {
  if (E.isStringEmpty(str) || !E.isStringisString(str)) {
    return errors.push("**" + fieldName + " is empty");
  }
  if (E.isStringLong(str, long)) {
    errors.push("**" + fieldName + " is too long");
  } else if (E.isStringShort(str, short)) {
    errors.push("**" + fieldName + " is too short");
  }
};
//*******VALIDATE DEAL REPORT******/
E.validateDealReport = (report,errors) => {
  E.validateUserName(report.publisher);
  E.validateUserName(report.reportedBy);
  E.validateString(report.reason,errors,'Reason',300,5);
  E.validateString(report.dealID,errors,'DealID',30,10);
}
//*****VALIDATE USER DATA FUNCTIONS*******************/
E.validateUserName = (username, errors) => {
  E.validateString(username, errors, "User name", 30, 12);
  if (!E.isStringIsEmail(username)) {
    //will return because why to check if its not an actual email
    return errors.push("**please insert a valid email address");
  }
};

E.isPasswordsMatch = (password1, password2, errors) => {
  if (!E.isStringsEqual(password1, password2)) {
    return errors.push("**passwords dont match");
  }
};

E.validatePassword = (password, errors) => {
  if (E.isStringEmpty(password) || !E.isStringisString(password)) {
    //will return because why to continue if its empty
    return errors.push("**password is empty");
  }
  if (!E.isPasswordIsAccordingToPoliciy(password)) {
    return errors.push(
      "**Password must be at least 8 charecters long+1 uppercase letter +1 number+1 symbol+1 lowercase letter"
    );
  }
};

//****internal function that using inside the main functions********************/
E.isStringEmpty = str => {
  if (
    !str ||
    str === "" ||
    str == null ||
    str == "undefind" ||
    str === undefined
  ) {
    return true;
  }
  return false;
};

E.isStringisString = str => {
  if (typeof str === "string") {
    return true;
  }
  return false;
};

E.isStringsEqual = (str1, str2) => {
  if (str1 === str2) {
    return true;
  }
  return false;
};

E.isStringLong = (str, length) => {
  if (str.length > length) {
    return true;
  }
  return false;
};

E.isStringShort = (str, length) => {
  if (str.length < length) {
    return true;
  }
  return false;
};

E.isStringIsEmail = str => {
  var pattern = RegExp(
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  return pattern.test(str);
};

E.isPasswordIsAccordingToPoliciy = password => {
  var re = RegExp(
    "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$"
  );
  console.info(re.test(password));
  return re.test(password);
};
