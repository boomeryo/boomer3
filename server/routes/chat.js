var express = require('express');
var router = express.Router();
const messages=require('../models/messagesModel')

router.io = require('socket.io')();

// //sockets
// //start listen with socket.io
router.io.on('connection', function (socket) {
    console.log('Connected to Sokcet!');
    
    // receive from client (index.ejs) with socket.on


    socket.on('createMessage', async function (msg) {
        socket.join(msg.chatID)
        //set time (temp)
        msg.time = new Date().getTime();
        //save the message to the DB
        let newMessage=new messages(msg)
        let messageSaved = await messages.createMessage(newMessage)
        if (messageSaved === true) {
            console.log('message Saved!')
            //if message was saved we are sending it to the room!
            router.io.to(msg.chatID).emit('newMessage',msg)
        }
    });
    
    socket.on('fetch', async function (chatID, fn) {
        //fetch the chat history and send it back to the client
        let messagesDb = await messages.getMessagesByChatID(chatID)
        fn(messagesDb)
    });

    // socket.on('join', (chatID) => {
    //     // if(!isRealstring(chatID)){
    //     //     callback('room id are required');
    //     // }
    //     socket.join(chatID, () => {
    //         let rooms = Object.keys(socket.rooms);
    //         console.log(rooms); // [ <socket.id>, 'room 237' ]
    //         router.io.to(chatID).emit('a new user has joined the room'); // broadcast to everyone in the room
    //       });
    //     console.log('emitted join!')
    // })


    socket.on('disconnect', () => {
        console.log('Disconnected from socket')
    })
});

// //sockets
// //start listen with socket.io
// router.io.on('connection', function (socket) {
//     console.log('User connected');

//     socket.on('disconnect', () => {
//         console.log('Disconnected from socket')
//     })

//     socket.emit('newMessage')
// });


    
module.exports = router;
