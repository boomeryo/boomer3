const  express = require('express');
const  router = express.Router();
const  Users=require ('../models/usersModel.js');
const validationService=require('../services/validationService');
const session = require('express-session');
const morgan =require('morgan');
const jwt = require('jsonwebtoken');
const authService = require('../services/authService')
const emailService=require('../services/mailService')
require('dotenv').config()

router.use(morgan('combined'));

//Express Session MiddleWare
router.use(session({
  secret: process.env.SESSION_SECRET,
  resave: true,
  saveUninitialized: true,
  cookie: {
    secure: false,
    maxAge: 432000000,
    //change it when we move to https
    httpOnly:false
  }
}));


//registration action
router.post('/register', async function (req, res) {
  try {
    let errors = [];
  //check the data inserted by the user
  userDetailsIsOk = validationService.validateUserSignUp(req.body, errors)
  if(userDetailsIsOk===false){
      return res.status(200).send(errors);
  }
  //check if the user already exisitng on DB
  userAlreadyExists = await Users.findUser({ username: req.body.username })
  if (userAlreadyExists!=null) {
    errors.push('**We already have user with that email, if its you, please reset your password or contact support')
    return res.status(200).send(errors)
  }
  //delete the second password
  delete req.body.password2
  req.body.active = true;
  req.body.type = 'user';
  req.body.token = 'null';
    //create the user to DB
     let newUser=new Users(req.body);
     let userCreated= await Users.createUser(newUser);
  if (userCreated) {
    console.log('userCreated');
    //send a welcome email to the user
    emailService.sendEmail('welcomeEmail',{ username: req.body.username })
       res.status(200).send(true)
    }
  } catch (error) {
    console.log(error)
    res.status(500).send(false)
  }
});


router.post('/login', authService.setTokenToReq, async function (req, res) {
  var errors = [];
  //data validation
  userDetailsIsOk = validationService.validateUserLogin(req.body, errors)
  if (userDetailsIsOk === false) {
    return res.status(200).send(errors)
  }

  let authUser = await loginUser(req.body, errors);
  let isAdmin= await authService.isUserAdmin(req.body.username)
  if (authUser === true) {
    console.log('user is active and authencitated with password')
    console.log('req Token is:'+req.token)
    if (req.token==='null') {
      //if no token (deleted cookie or cookie expired,but user is active and ok to login)
      let token = await jwt.sign({ username: req.body.username }, process.env.TOKEN_SECRET, { expiresIn: '7d' });
      await Users.updateUserById(req.body.username, { token: token })
      return res.status(200).send({isLoggedIn:true,token: token, username:req.body.username,isAdmin: isAdmin})
    } else {
      console.log('req token is not null')
      //check if verified and not blacklisted
      let tokenIsVerified = await authService.verifyToken(req.token)
      if (tokenIsVerified === true) {
        //if the user is verified but needed a new token
        console.log('your token is verified and you are registered user you can continue')
        return res.status(200).send({isLoggedIn:true, username:req.body.username, isAdmin: isAdmin})
      } else if (tokenIsVerified === false) {
          //if no token (deleted cookie or cookie expired,but user is active and ok to login)
          console.log('your token is expired and you are registered user you will get a new one')
          token = await jwt.sign({ username: req.body.username }, process.env.TOKEN_SECRET, { expiresIn: '7d' });
          return res.status(200).send({isLoggedIn:true,token: token, username:req.body.username, isAdmin: isAdmin})
      } else if (tokenIsVerified === true) {
        //has token and is ok to login
        console.log('your token is valid and nothing needs to be done')
        return res.status(200).send({ isLoggedIn:true, username:req.body.username,isAdmin: isAdmin })
      }
    }
  } else {
    return res.status(200).send(errors);
  }

});


//check if the user is blocked
router.post('/isBlocked', async function (req, res) {
  let userisblocked = await authService.isUserBlocked(req.body.username)
  res.status(200).send(userisblocked)
})

//check if the user is blocked
router.post('/isTokenBlacked', authService.setTokenToReq, async function (req, res) {
  console.log('req.token '+req.token)
  let userisblocked = await authService.isTokenBlacked(req.token)
  res.status(200).send(userisblocked)
})


//Login User
async function loginUser(user,errors){

  //holdes the user object form db
  let userFromDB = await Users.findUser({ username: user.username }, {});
  //check if tthe users exists
  if (userFromDB == null){
    errors.push('One of your details in incorrect/youre not registered');
    return errors
  }
  //check if the user is blocked and inactive
  else if(userFromDB.active===false){
    errors.push('Login failed, please contact support!');
    return errors;
  }

  //if everything ok with the user we will check the password now
  let validPassword = await Users.validateUserPassword(user.password, userFromDB.password);
  
  if(validPassword===true){
    //if the password is correct
    return true
    }
    else{
      //if the password is incorrect
      errors.push('one of your details is invalid');
      return errors
    }
  }



module.exports = router;
