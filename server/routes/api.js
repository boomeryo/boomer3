const express = require('express');
const router = express.Router();
const fs = require('fs');
const countries = require('../models/countryModel');
const categories = require('../models/categoryModel');
const deals= require('../models/dealsModel');
const cloudinary = require('cloudinary');
const fileUpload = require('express-fileupload');
const users = require('../models/usersModel.js');
const  messages = require ('../models/messagesModel.js');
const  chats = require ('../models/chatsModel.js');
const validator = require('validator');
const mailService = require('../services/mailService')
const authService = require('../services/authService')
const validationService = require('../services/validationService');
const blackListTokens=require('../models/blackListedTokensModel')
const jwt =require ('jsonwebtoken')
require('dotenv').config()


//initiate express files uploader
router.use(fileUpload());

//cloud configuraion
cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
})

//**************************************DEALS**********************************************************/
//****************************************************************************************************/
//***************************************************************************************************/

//add deal
router.post('/deals/add', authService.setTokenToReq, async function (req, res) {
    try {
        //verify the token!
        let verifyToken=await authService.verifyToken(req.token);
        if(verifyToken === false){
            return res.status(403);
        }

        let dealParsed = JSON.parse(req.body.deal)
            
        let errors=[];
        //validate pictures that user wants to upload
        let fileVal = validationService.validatePictures(req.files, errors);
        console.log('deal val:'+ fileVal)
        //check Deal body
        console.log('checking deal body...');
        let dealVal=validationService.validateDealCreation(dealParsed,errors)
        //if everything is ok
        if(fileVal && dealVal || errors.length==0){
        //upload files to server localy
        await uploadFilesToServer(req.files);
        //upload files to cloud and return array of metaData
        let uploadedToCloud= await uploadFilesToCloud(req.files,cloudinary)
        dealParsed.pictures= uploadedToCloud.pics
        //insert info needed for deal
        dealParsed.start=new Date();
        dealParsed.end = setEndDate(dealParsed.start, dealParsed.duration);
        dealParsed.disabled_by_admin = false
        dealParsed.disabled_reason='null'
        dealParsed.active=true;
        //delete the undefined picture values
        // deleteFieldsOnDeals(dealParsed);
        //delete temp files localy
        await deleteFilesFromServer(req.files,fs)
        //save the deal body to the DB
            console.log('writing to db now');
            console.log(dealParsed)
            if (uploadedToCloud.uploaded=== true) {
                console.log('creating deal on data base...')
                let newDeal=new deals(dealParsed);
                var dealCreated = await deals.createDeal(newDeal);
                console.log('deal Created')
            }
        if(dealCreated.created===true){
            console.log('deal created on database')
            res.status(200).send(dealCreated);
            console.log(dealCreated)
            mailService.sendEmail('postDeal', { username: dealParsed.publisher,dealID: dealCreated.dealID });
        } else {
            console.log(errors)
            res.status(200).send(errors);
        }
        } else {
            console.log(errors)
            res.status(200).send(errors);
            errors2=[];
        }
    } catch (err) {
        await deleteFilesFromServer(req.files,fs)
        console.log(err)
        res.status(500).send({created:false})
}

});


//return all the deals to the market (4 at one time, for the infinite scroll feature)
router.post('/deals/get', authService.setTokenToReq, async function (req, res) {
    try {
        //verify the token!
        let verifyToken=await authService.verifyToken(req.token);
        if(verifyToken === false){
            return res.status(403);
        }
        var deal= await deals.returnCollection(req.body.skipNum,req.body.filters);
        res.status(200).send(deal);
    } catch (error) {
        console.log(error)
        res.status(200).send(false)
    }
});


//Get Deal From DataBase
router.get('/deals/get/:id', authService.setTokenToReq, async function (req, res){
    try {
        let verifyToken = await authService.verifyToken(req.token);
        if (verifyToken === false) {
            return res.status(403)
        }
        let deal = await deals.getDealByID(req.params.id, true)
        let publisherDetails = await users.findUser({ username: deal.publisher }, {company:1 ,fullName:1, _id:0})
        console.log(deal)
        console.log(publisherDetails)
        if (deal && publisherDetails) {
            res.status(200).send({deal:deal, publisherDetails: publisherDetails});
        }
        else {
            res.status(200).send({nf:true});
        }
    }
    catch (e) {
        throw new Error('cannot get the deal from server')
     }
})

//**************************COUNTRIES**********************************************************************/
//fetch the country name list from DB
router.get('/countrylist',async  function(req, res) {
    var countryList=await countries.returnCollection2();
    return res.status(200).send(countryList);
});

//*********************************************CATEGORIES*****************************************************/
//fetch the category list
router.get('/categories/get',  async  function(req, res) {
    let categoryList = await categories.getCategories();
    res.status(200).send(categoryList);

});

//****************************SEARCH FILTERS SECTION*****************************************************/
router.post('/searchFilters/get:',authService.setTokenToReq,async function (req,res){
let verifyToken=await authService.verifyToken(req.token);
if(verifyToken === false){
    var filters=req.body.filters;
    var filteredDeals= await deals.returnCollection(filters);
}
})

//*****************************UPDATE DEAL******************************************************************/
router.post('/deals/update',authService.setTokenToReq,async function (req,res){
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    let errors = []
    let dealLocal = JSON.parse(req.body.deal)
    console.log(dealLocal)
    //check if the one who wants to edit the deal is the publisher
    let dealFromDB = await deals.getDealByID(dealLocal._id, dealLocal.active)
    console.log('deal fromDB *********')
    console.log(dealFromDB)
    //get the number of files (temp)
    let filesLength = 0
    for (x in req.files) {
        filesLength++
    }
    console.log('file length:'+filesLength)
    console.log('dealLocal: '+dealLocal.existingPictures.length )
    if (dealFromDB.publisher != dealLocal.publisher) {
        return res.status(403).send(false)
    }
    //change the deal end date if the number of days was changed
    if (dealFromDB.duration != dealLocal.duration) {
        console.log('updated the end date because the duration was changed!')
        dealLocal.end=setEndDate(dealLocal.start,dealLocal.duration)
    }
    switch (true) {
        case (filesLength > 0 && filesLength <= 5 && dealLocal.deletedPictures.length == 0):
            //when the user uploading new pictures without deleteing exsiting pictures (and add data or not)
            console.log('execute case 1!')
            // //upload new images to cloudinary and save them (if existing)
            validationService.validatePictures(req.files, errors);
            validationService.validateDealCreation(dealLocal, errors);

            if (errors.length<=0) {
                await uploadFilesToServer(req.files);
                // //upload files to cloud and return array of metaData
                let picturesArrayCloud = await uploadFilesToCloud(req.files, cloudinary);
                //save the details and new pics on db
                dealLocal.pictures = picturesArrayCloud.pics.concat(dealLocal.existingPictures)
                delete dealLocal.existingPictures
                delete dealLocal.deletedPictures
                let dealUpdated = await deals.updateDeal(dealLocal._id, dealLocal.publisher, dealLocal, { new: true });
                res.status(200).send(dealUpdated)
            } else {
                console.log('validation for case 1 failed', errors)
                return res.status(200).send(false)
            }
            break;

        case (filesLength > 0 && filesLength <= 5 && dealLocal.deletedPictures.length > 0):
            //when the user uploading new pictures with deleting exisitng images (and add data or not)
            console.log('execute case 2!')
            // //upload new images to cloudinary and save them (if existing)
            validationService.validatePictures(req.files, errors)
            validationService.validateDealCreation(dealLocal, errors)
            if (errors.length<=0) {
                //delete files
                let picsIsDeleted = await deleteDealPics(dealLocal.deletedPictures)
                console.log(picsIsDeleted)
                await uploadFilesToServer(req.files);
                // //upload files to cloud and return array of metaData
                let picturesArrayCloud = await uploadFilesToCloud(req.files, cloudinary);
                //save the details and new pics on db
                dealLocal.pictures = picturesArrayCloud.pics.concat(dealLocal.existingPictures)
                delete dealLocal.existingPictures
                delete dealLocal.deletedPictures
                let dealUpdated = await deals.updateDeal(dealLocal._id, dealLocal.publisher, dealLocal, { new: true });
                res.status(200).send(dealUpdated)
            } else {
                console.log('validation for case 2 failed', errors)
                return res.status(200).send(false)
            }
            break;
    
        case (dealLocal.deletedPictures.length > 0):
            //when the user deletes existing pictures but doesnt add new ones (and add data or not)!
            validationService.validateDealCreation(dealLocal, errors)
            if (errors.length<=0){
                console.log('execute case 3!')
                let picsIsDeleted = await deleteDealPics(dealLocal.deletedPictures)
                console.log('picture deleted?: ' + picsIsDeleted)
                console.log(dealLocal.existingPictures)
                console.log(dealLocal.deletedPictures)
                //save the details and new pics on db
                dealLocal.pictures = dealLocal.existingPictures
                delete dealLocal.existingPictures
                delete dealLocal.deletedPictures
                let dealUpdated = await deals.updateDeal(dealLocal._id, dealLocal.publisher, dealLocal, { new: true });
                res.status(200).send(dealUpdated)
            } else {
                    console.log('validation for case 3 failed', errors)
                    res.status(200).send(false)
            }
            break;
        case (filesLength == 0 && dealLocal.deletedPictures.length == 0):
            console.log(filesLength,dealLocal.deletedPictures)
            //when the user only updates data with no pictures
            if (validationService.validateDealCreation(dealLocal, errors)=== true) {
                console.log('execute case 4!')
                dealLocal.pictures = dealLocal.existingPictures
                delete dealLocal.existingPictures
                delete dealLocal.deletedPictures
                console.log(dealLocal)
                let dealUpdated = await deals.updateDeal(dealLocal._id,dealLocal.publisher,dealLocal,{new:true});
                res.status(200).send(dealUpdated)
            } else {
                console.log('validation for case 4 failed', errors)
                res.status(200).send(false)
            }
            break;
    }
    })



//*******************************************FUNCTIONS SECTION**************************************************/

 //will go through the files and save them to server in temp dir
 async function uploadFilesToServer(files){
     try {
        let promises=[]

        for (const key in files) {
            if (files.hasOwnProperty(key)) {
                var file= files[key];
                var filePath='./uploads/'+file.name.split('.')[0]+'.'+file.mimetype.split('/')[1];
                var upload=file.mv(filePath)
                promises.push(upload);
            }
        }
        Promise.all(promises).then(err=>{
           
        })
         
     } catch (err) {
         throw new Error("couldnt upload files to local server")
     }
}


//will upload files to the cloud server.
async function uploadFilesToCloud(files,cloudinary){
    try {
        console.log('starting to upload pictures to cloudinary!')
        let promises=[]
        let file, filePath, upload, pp, key
        for ( key in files) {
            console.log('entered Loop')
            if (files.hasOwnProperty(key)) {
                file = files[key];
                filePath = './uploads/' + file.name.split('.')[0] + '.' + file.mimetype.split('/')[1];
                upload = cloudinary.uploader.upload(filePath,{public_id:'deal1/bill'})
                promises.push(upload);
                console.log('picture inserted')
            }
        }
        pp = await Promise.all(promises)
        console.log(pp)
        console.log('upload to cloud is done!')
        picturesMeta = pp
        return { uploaded: true, pics: picturesMeta };
        
    } catch (err) {
        console.log(err)
        throw new Error('failed to upload images to cloud')
        
    }
   

}

 //will go through the files and save them to server in temp dir
 function deleteFilesFromServer(files,fs){
     try {

        for (const key in files) {
            if (files.hasOwnProperty(key)) {
                var file= files[key];
                var filePath='./uploads/'+file.name.split('.')[0]+'.'+file.mimetype.split('/')[1];
                console.log(filePath);
                fs.unlink(filePath,err=>{if (err) throw err;})
            }
         }
         console.log('done deleting files!');

                // Promise.all(promises).then(err=>{
                //     if(err) throw err;
                // })
         
     } catch (err) {
         throw new Error('failed to delete files from local server')
     }
}

//calculate the end date for the deal
function setEndDate(startDate,days) {
    if (days!='10' && days!='20' &&days!='30'){
      return;
    }
    var date2=new Date(startDate);
    date2.setDate(date2.getDate()+Number(days));

    return date2;
}


// function deleteFieldsOnDeals(deal){
//     numberOfPics=deal.numberOfPics;

//     for (const key in deal) {
//         if (deal.hasOwnProperty(key)) {
//             const element = deal[key];
//             if(element=='undefined'){
//                 delete deal[key];
//             }
//         }
//     }
// }

//*****************************************END OF DEALS SECTION***********************************************************/
//***********************************************************************************************************************/
//**********************************************************************************************************************/


//*****************************************USER CONTROL PANNEL SECTION************************************************/
//*******************************************************************************************************************/
//******************************************************************************************************************/


//handle reset password request
router.post('/users/passwordReset', async function (req, res) {
    let errors = [];
    //verify what the user entered is email or if the user doesnt exist
    validationService.validateUserName(req.body.username,errors)
    let userExist = await users.findUser({ username: req.body.username }, {})
    if (errors.length > 0 || !userExist || userExist == null) {
        return res.status(200).send({ success: false, errors: errors })
    }
    //get
    //create token with 1 hour experation time and save it to the DB
    let resetPasswordToken = jwt.sign({ data: req.body.username }, process.env.JWT_RESETPASS_SECRET, { expiresIn: Date.now() + 3600000 })
    let updatedUser = await users.updateUserById(req.body.username, {resetPasswordToken: resetPasswordToken,
                                                                    resetPasswordExpires: Date.now() + 3600000
                                                                    })
    if (updatedUser) {
        mailService.sendEmail('resetPassword', { username: req.body.username, link: `${process.env.APP_URL}/#/reset/?token=${resetPasswordToken}` })
        errors.push('If this email is associated with one of our users, an reset password email has been sent to this address.')
        return res.status(200).send({ success: true, errors: errors })
    } else {
        res.status(200).send({ success: false, errors: errors })
    }
});

//reset the user password
router.post('/users/reset/:token', async function (req, res) {

    try {
        let errors = [];
        console.log(req.body)
        //find the user that has the token
        let userWithToken = await users.findUser({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } })
        if (!userWithToken) {
            return res.status(403).send(false)
        }
        //check if the passwords are ok and equal
        let passwordsValidated = validationService.validatePasswordReset(req.body, errors)
        //check if 2 passwords are equal
        if (passwordsValidated === true) {
            let passwordChanged = await users.changeUserPasswordByReset(userWithToken.username, req.body.password1)
            if (passwordChanged === true) {
                mailService.sendEmail('changedPassword', { username: userWithToken.username })
                return res.status(200).send(true)
            } else {
                return res.status(500).send(false)
            }
        }
        res.status(200).send(errors)
    } catch (error) {
        console.log(error)
        return res.status(500).send(false)

    }
    
});



//change user password
router.post('/users/changeUserPassword', authService.setTokenToReq, async function (req, res) {
    try {
            let verifyToken=await authService.verifyToken(req.token);
        if (verifyToken === false) {
            return res.status(403)
        }
        let errors = [];
        console.log(req.body)
        //validate the current password
        let currentPasswordOk = validationService.validatePasswordUpdate(req.body,errors)
        if (currentPasswordOk===true) {
            //update the password
            //change the password on db
            await users.changeUserPassword(req.body)
            mailService.sendEmail('changedPassword', {username:req.body.username})
            res.send(errors)
        } else {
            res.status(200).send(errors)
        }
    } catch (error) {
        let errors = [];
        errors.push(error)
        console.log(error)
        res.status(500).send(errors)
    }
    
});


//get the user info for the cp
router.post('/users/getUserInfo', authService.setTokenToReq, async function (req, res) {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    let userInfo = await users.findUser({ username: req.body.username }, { country: 1, fullName: 1, company: 1,countinent: 1 })
    console.log(userInfo)
    res.status(200).send(userInfo);
});

//Update user details
router.post('/users/updateUserInfo', authService.setTokenToReq, async function (req, res) {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    var errors = [];
    console.log(req.body.newInfo)
    //validation
    if (validationService.validateUserUpdateDetails(req.body, errors) === true) {
            //update the info
    let userInfoUpdated = await users.updateUserById(req.body.username, req.body)
    // console.log(userInfoUpdated)
    if (!userInfoUpdated) {
        return res.status(500).send(false)
    }
    return res.status(200).send(true)
    }
    res.status(500).send(errors)

});

//get all deals posted by the user
router.post('/users/getUserDeals', authService.setTokenToReq, async function (req, res) {
    try {
        let errors=[]
        let verifyToken=await authService.verifyToken(req.token);
        if (verifyToken === false) {
            return res.status(403)
        }
        validationService.validateUserName(req.body.publisher,errors)
        if (errors.length>0) {
            console.log(errors)
            return res.status(200).send(false)
        }
        let userDeals = await deals.getDealsByUserName(req.body.publisher)
        res.status(200).send(userDeals)
    } catch (error) {
        console.log(error)
        res.status(500).send(null)
    }
})


//Deactivate Deal
router.post('/deals/changeDealStatus', authService.setTokenToReq, async function (req, res) {
    try {
        let verifyToken=await authService.verifyToken(req.token);
        if (verifyToken === false) {
            return res.status(403)
        }
        let Duration = new Date();
        let data = { active: false, end: Date.now() }
        if (req.body.dealStat === 'reactivate') {
            data = { active: true, start: Date.now(), end:Duration.setDate(Duration.getDate()+parseInt(req.body.duration))}
        }
        let dealStatusChanged = await deals.updateDeal(req.body.dealID, req.body.username, data, {new:true})
        console.log("deal status changed: "+dealStatusChanged)
        if (dealStatusChanged === true) {
            return res.status(200).send(true)
        } else {
            console.log('omer')
            return res.status(200).send(false)
        }
    } catch (error) {
        res.status(200).send(false)
    }
    
})

//Delete Deal
router.post('/deals/deleteDeal', authService.setTokenToReq, async function (req, res) {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    let deletedDeal = await deleteDeal(req.body.dealID, req.body.username)
    console.log(deletedDeal)
    if(deletedDeal===true){
        return res.status(200).send(true)
    }
    res.status(200).send(false)
})


//need to work on this section
async function deleteDealPics(picsArray) {
    var promises = []
    for (let i = 0; i < picsArray.length; i++) {
        promises.push(cloudinary.v2.uploader.destroy(picsArray[i].public_id))
    }
    let deletedPics=await Promise.all(promises).then(function (result) {
                                            return result
    })
    let deleted = true
    //will check if array have 'not found' err 
    for(var i = 0; i < deletedPics.length; i++) {
        if (deletedPics[i].result == 'not found') {
            deleted=false;
            break;
        }
    }
    return deleted
}

async function deleteDeal(dealID, username) {
    //get the deal pic public ids for delete
    let deal = await deals.getDealByID(dealID);
    if (deal==null) {
        return 'err'
    }
    // delete the deal and the pics
    let picsIsDeleted=await deleteDealPics(deal.pictures)
    let dealIsDeleted = await deals.deleteDeal({_id: dealID , publisher: username})
    if (dealIsDeleted === true && picsIsDeleted === true) {
        return true
    }
    return false
}

//ValidatePassword
function validatePassword(errors, password){
    // let empty=validator.isEmpty(password)
    if (validator.isEmpty(password)){
        errors.push("**Password is empty!")
        return false
    }
    else{
      //8 charecters long, 1 special charecter, 1 upper case letter, 1 lower case letter, at least 1 number
      let reg=RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
      if(!reg.test(password))
      {
          errors.push("Password are Not Maching Our Rules")
          return false

        }
        return true
    }
}

function validateUpdatedInfo(newInfo, errors) {
    let errors = [];
    //chek the info
    if (!validator.isEmpty(newInfo.username) || !validator.isEmpty(newInfo.country) || !validator.isEmpty(newInfo.company)) {
        errors.push('one of the fields are empty')
        return errors
    }
    return errors
}

//*****************************************CHAT/INBOX SECTION*********************************************************/
//*******************************************************************************************************************/
//******************************************************************************************************************/

//get chat messages
router.get('/chats/:chatID', authService.setTokenToReq, async (req, res) => {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    
    let messagesFromDB = await messages.getMessagesByChatID(req.params.chatID)
    if (!messagesFromDB || messagesFromDB.length <= 0) {
        return res.status(200).send(messagesFromDB)
    }
    console.log(messagesFromDB)
    return res.status(200).send(messagesFromDB)
})

//create a chat
router.post('/chats/createChat', authService.setTokenToReq, async (req, res) => {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    if (req.body.message || req.body.message != null) {
        //check if the user send message to himself
        console.log(req.body)
        console.log(req.body.to)
        console.log(req.body.from)
        if (req.body.to === req.body.from) {
            console.log('omer!!!')
            return res.status(200).send(false)
        }
        //check if there is exisitng chat
        //provide the message to the function to so it could be created if there is exisitng chat
        let chatCreated = await chats.createChatOrInsertMessageToChat(req.body)
        console.log('omer')
        console.log(chatCreated)
        if (chatCreated === false) {
            return res.status(500)
        }
        return res.status(200).send(true)
    }
        return res.status(200).send(false);
})

//get user Chats
router.get('/chats/getChats', authService.setTokenToReq, async (req, res) => {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    
    let chatsFromDB = await chats.getMessagesByChatID(req.body.username)
    if (!chatsFromDB || chatsFromDB.length <= 0) {
        return res.status(200).send(false)
    }
    return chatsFromDB
})

//add chat message
router.post('/chats/createMessage', authService.setTokenToReq, async (req, res) => {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }

    let newMessage=new messages(req.body)
    let messageCreated = await messages.createMessage(newMessage)
    if (messageCreated===false) {
        return res.status(200).send(false)
    }
    return res.status(200).send(true)
})

//get chat list by username
router.post('/chats/:username/getChats', authService.setTokenToReq, async (req, res) => {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    let chatsList = await chats.getchatByUserName(req.params.username)
    console.log(chatsList)
    if (!chatsList || chatsList.length <= 0) {
        return res.status(200).send(false)
    }
    return res.status(200).send(chatsList)
})


//*****************************************MAIN PAGE SECTION**********************************************************/
//*******************************************************************************************************************/
//******************************************************************************************************************/

//get 3 latest deal to main page
router.get('/mainPage/getDeals',authService.setTokenToReq, async function(req, res){
    // let verifyToken=await authService.verifyToken(req.token);
    // if (verifyToken === false) {
    //     return res.status(403)
    // }
    let dealsFromDB= await deals.getMainPageDeals();
    if(dealsFromDB===false){
        return res.status(200).send(false); 
    }
    res.status(200).send(dealsFromDB);
});

//send contact message
router.post('/sendContactMessage', function (req, res) {
    console.log(req.body)
    let errors=[]
    validationService.validateContactMessage(req.body, errors);
    console.log(errors)
    if (errors.length <= 0) {
        console.log('im true@!')
        mailService.sendEmail('contactMessage',req.body)
        mailService.sendEmail('contactMessageApproval', { username: req.body.email })
        res.status(200).send(true)
    } else {
        console.log('im false!')
        res.status(200).send(false)
    }
})


//***********************************************************************************************************/
//**************************************REPORT DEAL SECTION**************************************************/
//***********************************************************************************************************/

router.post('/reportDeal', authService.setTokenToReq, async function (req, res) {
    let verifyToken=await authService.verifyToken(req.token);
    if (verifyToken === false) {
        return res.status(403)
    }
    let errors = []
    validationService.validateDealReport(req.body, errors)
    if (errors.length <= 0) {
        mailService.sendEmail('reportEmail',req.body)
        return res.status(200).send({ success: true })
    }
    return res.status(200).send({ success: true, errors: errors })
})


//***********************************************************************************************************/
//**************************************ADMIN SECTION********************************************************/
//***********************************************************************************************************/

//gel all users collection
router.post('/admin/getUsers', authService.setTokenToReq, async function (req, res) {
    console.log(req.body)
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    let userList = await users.findUsers({ username: req.body.user }, {})
    console.log(userList)
    if (userList!=null) {
        return res.status(200).send(userList)
    }
    res.status(500).send(false)
})

//block user
router.post('/admin/blockUser', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    let queryData = { active: false, disabled_reason:req.body.reason }
    //if admin want to unblock the user.
    if (req.body.action === 'unblock') {
        queryData.disabled_reason=null
        queryData.active=true
    } else {
        var tokenIsBlacked = await authService.blackListToken(req.body.userID)
    }
    let userIsBlocked = await users.updateUserByIdSet(req.body.userID, queryData)
    console.log(userIsBlocked)
    if (userIsBlocked === true && tokenIsBlacked===true && req.body.action!='unblock') {
        console.log('user was updated')
        return res.status(200).send(true)
    } else if (userIsBlocked === true) {
        console.log('user was unblocked')
        return res.status(200).send(true)
    }
    console.log('failed to unblock/block user')
    res.status(500).send(false)
})


//remove user(maybe also his info? and deals?)
router.post('/admin/removeUser', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    let tokenIsBlacked = await authService.blackListToken(req.body.userID)
    let userIsDeleted = await users.removeUser(req.body.userID)

    if (userIsDeleted===true && tokenIsBlacked===true) {
        //user is removed
        return res.status(200).send(true)
    }
    res.status(500).send(false)
})

//deactivate deal
router.post('/admin/deactivateDeal', authService.setTokenToReq, async function (req, res) {
    try {
        let verifyToken = await authService.verifyToken(req.token);
        let isAdmin = await authService.isUserAdmin(req.body.username);
        if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
            return res.status(403)
        }
        let queryData = {
            active: false,
            end: Date.now(),
            disabled_by_admin: true,
            disabled_reason: req.body.reason
        }
        if (req.body.action === 'reactivateDeal') {
            queryData.active=true
            queryData.disabled_by_admin = false
            queryData.disabled_reason = null
            delete queryData.end
            queryData.start = Date.now();
        }
        let dealIsDeactivated = deals.updateDealBySet({ _id: req.body.dealID },queryData)
        if (dealIsDeactivated != null) {
            if (req.body.action === 'reactivateDeal') {
                mailService.sendEmailToUser(req.body.publisher, req.body, 'dealAdminReactivation')
            } else {
                mailService.sendEmailToUser(req.body.publisher, req.body, 'dealAdminDeactivation')
            }
            return res.status(200).send(true)
        }
        res.status(500).send(false)
    } catch (error) {
        res.status(500).send(false)
    }
})


//remove deal
router.post('/admin/removeDeal', authService.setTokenToReq, async function (req, res) {
    try {
        let verifyToken = await authService.verifyToken(req.token);
        let isAdmin = await authService.isUserAdmin(req.body.username);
        if (isAdmin === false && verifyToken === false || isAdmin === true && verifyToken === false || isAdmin === false && verifyToken === true) {
            return res.status(403)
        }
        let dealPictures = await deals.searchDeal({ _id: req.body.dealID }, { pictures: 1, _id: 0 })
        let picsDeletedFromCloud = await deleteDealPics(dealPictures[0].pictures);
        let dealIsDeleted = await deals.deleteDeal({ _id: req.body.dealID })
        console.log(picsDeletedFromCloud)
        console.log(dealIsDeleted)
        if (picsDeletedFromCloud === true && dealIsDeleted === true) {
            console.log('Deal Was Deleted')
             res.status(200).send(true)
        } else {
            res.status(500).send(false)
        }
    } catch (error) {
        console.log(error)
        res.status(500).send(false)

    }
})

//data of deals and users and more
router.post('/admin/generaldata', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    //blockedUsers
    //activeUsers
    //newUsers

    //activeDeals
    //inactiveDeals
    //newDeals
    let dealsData = await deals.dealsGeneralDataForAdmin();
    let usersData = await users.usersGeneralDataForAdmin();
    res.status(200).send({ usersData, dealsData })
})

//data of deals and users and more
router.post('/admin/deals/customData', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    let data = await deals.dealsBiData(req.body.query)
    res.status(200).send(String(data))
})

//data of deals and users and more
router.post('/admin/deals/searchDeals', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    console.log(req.body.deal)
    //drop the spaces
    let deal = await deals.searchDeal(req.body.deal)
    res.status(200).send(deal)
})

//add category
router.post('/admin/categoryAdd', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    let categoryAdded = await categories.addCategory(req.body.category)

    res.status(200).send(categoryAdded)
})

//delete category
router.post('/admin/categoryDelete', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken === false || isAdmin === true && verifyToken === false
        || isAdmin === false && verifyToken === true){
        console.log(isAdmin)
        return res.status(403)
    }
    let categoryDeleted = await categories.deleteCategory(req.body.catID)

    res.status(200).send(categoryDeleted)
})

//add category
router.post('/admin/categoryUpdate', authService.setTokenToReq, async function (req, res) {
    let verifyToken = await authService.verifyToken(req.token);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    if (isAdmin === false && verifyToken===false || isAdmin===true && verifyToken===false || isAdmin===false && verifyToken===true) {
        console.log(isAdmin)
        return res.status(403)
    }
    let categoryUpdated = await categories.updateCategory(req.body.catID, req.body.newCategoryName)
    res.status(200).send(categoryUpdated)
})

//check if the user is admin and have valid token
router.post('/admin/isAdmin', authService.setTokenToReq, async function (req, res) {
    console.log(req.body)
    let verifyToken = await authService.verifyToken(req.token,req.body.username);
    let isAdmin = await authService.isUserAdmin(req.body.username);
    console.log(verifyToken,isAdmin)
    if (isAdmin === true && verifyToken===true|| isAdmin===true && verifyToken.verified===true) {
        return res.status(200).send(true);
    }
    res.status(403).send(false)
})




module.exports = router;
