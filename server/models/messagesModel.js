const mongoose = require('mongoose');

// Deal Schema
var messagesSchema = mongoose.Schema({
    chatID: { type: String, required: true, },
    from: { type: String, required: true, },
    to: {type:String, required:true},
    message: { type: String, required: true },
    time: {type: Date,required: true,default: new Date().getTime()},
});


var messages = module.exports = mongoose.model('messages', messagesSchema);


//return messages by id
module.exports.getMessagesByChatID = async (chatID) => {
    try {
        let messagesdb = await messages.find({ chatID: chatID }).limit(50).sort({ time: 1 })
        return messagesdb
    } catch (error) {
        throw new Error('couldnt get the messages you asked for!')
    }
}


//add chat/message
module.exports.createMessage = async (messageObj) => {
    try {
        let messageInserted = await messageObj.save()
        console.log(messageInserted)
        if (!messageInserted) {
            console.log('omer omer')
            console.log(messageInserted)
            return false
        }
        console.log('omer omer')
        return true
    } catch (error) {
        throw new Error('coudlnt insert this message!')
    }
} 