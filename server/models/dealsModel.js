const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const E = module.exports;

// Deal Schema
var DealSchema = mongoose.Schema({
    title: {type: String,required: true,},
    description: {type: String,required: true},
    duration: {type: String,required: true},
    category: {type: String,required: true},
    start: {type: Date,required: true},
    end: {type: Date,required: true},
    country: { type: String, required: true },
    continent:{type:String, required:true },
    openForBids: {type: Boolean,required: true},
    publisher: { type: String,required: true},
    active: { type: Boolean, required: true },
    disabled_by_admin: { type: Boolean, required: true, default: false },
    disabled_reason: {type: String, required: true, default: null},
    pictures: {type: [], required: true}
});

//For mongoose paigin package (and infinite Scroll)
DealSchema.plugin(mongoosePaginate)

var deal = module.exports = mongoose.model('deals', DealSchema);


//return the collection 4 at the time for the infinite scrolling feature add filters if needed
module.exports.returnCollection = async function (skipNum, f) {
    try {
        //if not have
        let query = {active:true}
        //if filters was not given
        if (!f || f === 'undefined' || f == null) {
            //will leave query as it is
        } else {
            for (const key in f) {
                    if (f.hasOwnProperty(key)) {
                        const element = f[key];
                        if(Array.isArray(element)){
                            if(key==='category'){
                                if(element.length>0){//if the element array will check if its empty
                                    query.category={$in:element}
                                }
                            }else if(element.length>0){//if the element array will check if its empty
                                query.continent={$in:element}
                            }
                        }
                        if(typeof element===Boolean){//will check for only true values
                            // if(key==='notMoney' && element===true){
                            //     query.notMoney=element
                            // }
                            // else if(key==='onlyMoney' && element===true){
                            //     query.onlyMoney=element
                            // }
                            if(key==='openForBids' && element===true){
                                query.openForBids=element
                            }
                        }
                        if (typeof element === 'string' && element != '') {
                            //will check if string is empty
                            query.country=element
                        }
                    }
            }
        }

        let deals = await deal.paginate(query, {page: skipNum,limit: 4,sort: {start: -1}})
        return deals.docs; //the .docs is a paginate thing

    } catch (err) {
        console.log(err)
        throw new Error("Coudlnt bring the deals from data base")
    }
}

//Get Deal By ID (for the deal page)
module.exports.getDealByID = async (dealID,active) => {
    try {
        let query;
        if (active) {
            query={_id:dealID, active:active}
        }
        else {
            query={_id:dealID}
        }
        return await deal.findOne(query).then(deal => {
            if (!deal) {
                //if no active deal was found
                return deal;
            }
            //if deal was found
            return deal;
        })    
    } catch (e) {
        throw new Error('Had a problem getting deal from data base')
    }
}

//Create new deal record
module.exports.createDeal = async function (newDeal) {
    try {
        let dealCreated = await newDeal.save();
        console.log(dealCreated)
        //will show the result
        if (dealCreated) {
            return {created: true, dealID: dealCreated._id};
        }
        return false;
    } catch (error) {
        throw new Error(error)
    }
};


//Get Active Deals By ID
//if user not exists or not formatted, will return error
E.getDealByUser = async function (username) {
    try {
        let deals = await deal.find({ publisher: username })
        if (deals.length <= 0) {
            return null
        }
        return deals
    } catch (error) {
        throw new Error(error)
    }
}

module.exports.getDealsByUserName = async function (username) {
    try {
        let deals = await deal.find({ publisher: username })
        if (deals.length <= 0) {
            return null
        }
        return deals
    } catch (error) {
        throw new Error(error)
    }
}

//update deal, using also for delete deal (make it not active)
module.exports.updateDeal = async function (dealId,username, newData, options) {
    try {
        let dealWasUpdated = await deal.findOneAndUpdate({ _id: dealId, publisher: username, disabled_by_admin: false }, newData, options)
        if (dealWasUpdated) {
            return true;
        }
        return false
    } catch (error) {
        throw new Error('Didnt Update The Deal!')
    }
};

//new add deal function
module.exports.updateDealBySet = async (dealID,query)=> {
    try {
        let dealUpdated = await deal.update({ _id: dealID }, { $set: query })
        console.log('dealUpdate:' +dealUpdated)
        if (dealUpdated.nModified == 1) {
            console.log('deal was updated!')
            return true
        }
        console.log('deal was not updated!')
        return false
        
    } catch (error) {
        throw new Error('Cant Update The Deal')
    }
}

//Remove Deal 
module.exports.deleteDeal = async function (query) {
    try {
        let deletedDeal= await deal.findOneAndDelete(query)
        if(deletedDeal){
            return true
        }
        return result;
    }catch(error){
        throw new Error('Can not delete deal!'+error)
    }
};

//Remove Deal-bulk
module.exports.deleteDealsBulk = async function (query) {
    try {
        let deletedDeals = await deal.deleteMany(query)
        if(deletedDeals){
            return true
        }
        return result;
    }catch(error){
        throw new Error('Can not delete deals!'+error)
    }
};

//Get the main page deals by the latest date
module.exports.getMainPageDeals = async function(){
    try{
        let deals = await deal.find({active: true}).sort({start:-1}).limit(3)
        if(!deals || deals.length==0){
            return false
        }
        return deals
    }catch(error){
        throw new Error('Can not load deals!')
    }
}

//deal numberic data
module.exports.dealsGeneralDataForAdmin = async () => {
    try {
        let start = new Date();
        start.setHours(0,0,0,0);
        let end = new Date();
        end.setHours(23,59,59,999);
        let data = {}
        data.activeDeals = await deal.find({ active: true }).count()
        data.inactiveDeals = await deal.find({ active: false }).count()
        data.newDealsByDay = await deal.find({ start: { $gte: start,$lt: end } }).count()
        data.disabledDealsByAdmin = await deal.find({ active: false,disabled_by_admin:true}).count()
        data.disabledDealsByUser = await deal.find({ active: false, disabled_by_admin: false }).count()
        console.log(data)
        return data
    } catch (error) {
        throw new Error(error)
    }
}

//will fetch the amount of any query data
module.exports.dealsBiData = async (data) => {
    try {
        console.log(data)
        return await deal.find(data).count()
    } catch (error) {
        throw new Error('cannot fetch that data.')

    }
}

module.exports.searchDeal = async (query,options) => {
    try {
        dealFromDB = await deal.find(query,options)
        return dealFromDB
    } catch (error) {
        throw new Error('cant fetch deal!')
    }
}
