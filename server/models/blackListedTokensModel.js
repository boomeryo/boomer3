const mongoose = require('mongoose');
const E = module.exports;

// Deal Schema
var blackListedTokens2 = mongoose.Schema({
    token:{type: String, required:true }
});

var blackListedTokens = module.exports = mongoose.model('blackListedTokens', blackListedTokens2);


module.exports.findToken = async function (token) {
    try {
        let tokenIsBlocked = await blackListedTokens.findOne({ token: token })
        if (tokenIsBlocked != null) {
            return true
        }
        return null
    } catch (error) {
        throw new Error(error)
    }
}

module.exports.insertToken = async function (token) {
    try {
        let exists = await blackListedTokens.findOne({ token: token });
        if (exists != null) {
            return true
        }
        let tokenInserted = await blackListedTokens.create({ token })
        if (tokenInserted.token === token) {
            return true
        }
        return false

    } catch (error) {
        throw new Error(error)
    }
}

module.exports.removeToken = async function (token) {
    return await blackListedTokens.findOneAndRemove({token:token})
}


