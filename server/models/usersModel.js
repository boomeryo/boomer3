const mongoose = require('mongoose');
var bcrypt = require("bcrypt")

// User Schema
var UserSchema = mongoose.Schema({
    username: {type: String, required: true, lowercase:true, unique: true},
    password: {type: String, required: true},
    fullName: {type: String, lowercase: true, required: true},
    company: {type: String, lowercase: true, required: false},
    phone: {type: String,  lowercase: true, required: false},
    intrests: {type: [],  lowercase: true, required: false},
    country: { type: String, required: true },
    continent:{type: String, required: true },
    type:{ type: String, lowercase: true, required:true },
    active: { type: Boolean, lowercase: true, deafult: true, required: true },
    disabled_reason: { type: String, lowercase: true, deafult: null },
    token:{type: String, required: true},
    resetPasswordToken: { type: String, default: null },
    resetPasswordExpires:{type: Date ,default: null},
    created: { type: Date, deafult: Date.now()},
  });

  var user = module.exports = mongoose.model('Users', UserSchema);

  //Create new user record
  module.exports.createUser= async function(newUser){
    let userPassword = newUser.password;
    //Hash user's password
    let hashedPassword = await bcrypt.hash(userPassword, 10)
    // Store hashed password in DB.
    console.log(hashedPassword);
    newUser.password = hashedPassword;
    var promise = newUser.save().then(result=>{
        //will show the result
        console.log(result);
        if (result){
            return true;
        }
        return false;
    });
    return promise;
};

//find users
module.exports.findUsers = async (query, options) => {
    try {
        let usersList = await user.find(query, options)
        if (!usersList) {
            return null
        }
        return usersList
    } catch (error) {
        throw new Error("error doing find users")
    }
}

//find user
module.exports.findUser = async (query, options) => {
    try {
        let userFromDB = await user.findOne(query,options)
        if (userFromDB != null) {
            return userFromDB
        }
        return null
    } catch (error) {
        throw new Error('Cannot preform the query, check the function'
        )
    }
}

//validatePassword
module.exports.validateUserPassword= async function(password,hash){
    return  await bcrypt.compare(password, hash)
}

//update user
module.exports.updateUserById = async function (userID, newInfo) {
    let userWasUpdated = await user.findOneAndUpdate({ username: userID }, newInfo , { new: true })
    return userWasUpdated
}

//temp
module.exports.updateUserByIdSet = async function (userID, newInfo) {
    try {
        let userUpdated = await user.updateOne({ _id: userID }, { $set: newInfo })
        if (userUpdated.nModified == 0) {
            return false
        }
        return true
    } catch (error) {
        throw new Error("didnt update user, check the details you provided")
    }
}


//change user password
module.exports.changeUserPassword = async function(data){
    try {
        //find the username and fetch his password
        let userObj = await this.findUser({ username: data.username });
        console.log(userObj)
        if (!userObj) {
            console.error('user:'+data.username+' not found')
            return false
        }
        //check if the password user typed equals to hashed one
        let checkPassword = await this.validateUserPassword(data.currentPassword, userObj.password)
        //if yes, create new hash with the new password and update the user record
        if (checkPassword===true) {
            let hashedNewPassword = await bcrypt.hash(data.newPassword1, 10)
            let passwordUpdated = await this.updateUserById(data.username, { password: hashedNewPassword })
            if (passwordUpdated != null) {
                console.log('password upadte was a success')
                return true
            }
            console.error('the password upadte didnt work')
            return false
        }
        else {
            return false
        }
        
    } catch (error) {
        throw new Error('wasnt ebale to change users password!')
    }
}

//change user password by reset password
module.exports.changeUserPasswordByReset = async function (username,newPassword) {
    try {
        let hashedNewPassword = await bcrypt.hash(newPassword, 10)
        let passwordChanged = await this.updateUserById(username, { password: hashedNewPassword })
        if (passwordChanged.password === hashedNewPassword) {
            console.log('password Changed')
            return true
        } else {
            return false
        }
    } catch (error) {
        throw new Error('wasnt ebale to reset users password!')
    }
}

//remove user from db by username
module.exports.removeUser = async function (userID) {
    try {
        let userIsDeleted = await user.findOneAndRemove({ _id: userID })
        console.log(userIsDeleted)
        if (userIsDeleted != null) {
            return true
        }
        return false
    } catch (error) {
        throw new Error('failed to delete user')
    }
}


module.exports.usersGeneralDataForAdmin = async () => {
    try {
        let start = new Date();
        start.setHours(0,0,0,0);
        let end = new Date();
        end.setHours(23,59,59,999);
        let dataUsers = {}
        dataUsers.activeUsers = await user.find({ active: true }).count()
        dataUsers.blockedUsers = await user.find({ active: false }).count()
        dataUsers.newUsersToday = await user.find({ created: { $gte: start, $lt: end } }).count()
        console.log(dataUsers)
        return dataUsers
    } catch (error) {
        throw new Error(error)
    }
}
