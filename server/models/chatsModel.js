const mongoose = require('mongoose');
const messages = require('./messagesModel')
// chats Schema
var chatsSchema = mongoose.Schema({
    users:{type:[] ,required:true}
});


var chats = module.exports = mongoose.model('chats', chatsSchema);


//return chats by id
module.exports.getchatByChatID = async (chatID) => {
    try {
        let chat = await chats.find({ _id: chatID }).limit(50).sort({ time: -1 })
        return chat
    } catch (error) {
        throw new Error('couldnt get the chats you asked for!')
    }
}

//get chats by user name
module.exports.getchatByUserName = async (username) => {
    try {
        let chat = await chats.find({ users: { $in: [username] } })
        return chat
    } catch (error) {
        throw new Error('couldnt get the chat you asked for!')
    }
}

//add chat
module.exports.createChat = async (chatObj) => {
    try {
        console.log(chatObj)
        let chatInserted = await chatObj.save()
        console.log(chatInserted)
        if (!chatInserted || chatInserted==null) {
            return false
        }
        return { created: true, id: chatInserted._id }
    } catch (error) {
        throw new Error('coudlnt Create Chat')
    }
}

module.exports.createChatOrInsertMessageToChat = async (message) => {
    let messageCreated;
    let newMessage;
    //check if there is an exisitng chat
    let chatExisitng = await chats.findOne({ users: [message.from, message.to] })
    console.log(chatExisitng)
    //if there is an exisitng chat
    if (chatExisitng || chatExisitng != null) {
        //there is a chat so we just need to insert a message to it!
        message.chatID = chatExisitng.id
        newMessage = new messages(message)
        console.log(newMessage)
        messageCreated = await messages.createMessage(newMessage)
        if (messageCreated === true) {
            console.log('Existing chat, message Created')
            return true
        }
        console.log('Exisitng chat ,Create Message Failed!')
        return false
    }
    //if chat is not already existed, we will create it and then will insert the message!
    //create the chat and get his id!
    //build the chat object
    let chatObj=new chats({users:[message.from,message.to]})
    let newChat = await chats.createChat(chatObj)
    if (newChat.pass = true) {
        message.chatID = newChat.id
        newMessage = new messages(message)
        console.log(newMessage)
        messageCreated = await messages.createMessage(newMessage)
        if (messageCreated === true) {
            console.log('new chat, message Created')
            return true
        }
        console.log('new chat,Create Message Failed!')
        return false
    }
}