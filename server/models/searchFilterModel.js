const mongoose = require('mongoose');
var E=module.exports;

// User Schema
var searchFilterSchema = mongoose.Schema({
    Filter: {type: String, required: true,lowercase:true, unique: true},
    options: {type: [], required: true},
  });

  var searchFilter = module.exports = mongoose.model('SearchFilters', searchFilterSchema);

  //will fetch the category  collection
  module.exports.returnCollection=function(){
      
      let searchFilters=searchFilter.find().then(searchFilters=>{
          if(err){throw err};
          if(!searchFilters){return 'no categories'};
          return searchFilters; 
      }).catch(err=>{
          return err
          console.log(err);
  });
  return searchFilters;
};


//create category
module.exports.createFilter=function(newSearchFilter){
    let filter=newSearchFilter.save().then(searchFilter=>{
        if(searchFilter){
            return true;
        }
        return false;
    }).catch(err=>{
        console.log('cant create search filter');
        return false;
    });
    return filter
}

//delete category
module.exports.deleteFilter=function(filter){
    var updateFilter=searchFilter.findOneAndUpdate({_id: filter.id},filter,{returnDocument:true}).then(filter=>{
        if(!filter){
            return false;
        }
        return true;
    }).catch(err=>{
        console.log("error");
        return false;
    })
    return updateFilter;
}
//update category

