const mongoose = require('mongoose');
var E=module.exports;

// User Schema
var categoriesSchema = mongoose.Schema({
    category_name: {type: String, required: true, lowercase:true, unique: true},
  });

  var categories = module.exports = mongoose.model('categories', categoriesSchema);

  //will fetch the category  collection
  module.exports.getCategories= async () => {
      try {
          let categoryList = await categories.find({})
          if (categoryList.length>0) {
              return categoryList
          }
          return null
          
      } catch (error) {
          throw new Error('failed to bring categories from db')
      }
};


//create category
module.exports.addCategory = async (category) =>{
    try {
        if (!category) {
            return false
        }

        let categoryAdded = await categories.create({ category_name: category })
        if (categoryAdded._id) {
            return true
        }
        return false
    } catch (error) {
        console.log('category exisiting or invalid')
        return false
    }
}

//delete category
module.exports.deleteCategory = async (catID)=> {
    try {
        if (!catID) {
            return false
        }
        let categoryDeleted = await categories.findOneAndRemove({ _id: catID })
        if (categoryDeleted != null) {
            console.log('the category was deleted!')
            return true
        }

        return false
    } catch (error) {
        throw new Error('cannot delete category: '+error)
    }
}

//delete category
module.exports.updateCategory = async (category,newCategoryName)=> {
    try {
        if (!category) {
            return false
        }
        console.log(category)
        let categoryUpdated = await categories.findOneAndUpdate({ _id: category },{category_name: newCategoryName},{new:true})
        console.log(categoryUpdated)
        if (categoryUpdated != null && categoryUpdated.category_name === newCategoryName) {
            console.log('category was updated')
            return true
        }
        console.log('category wasnt updated')
        return false
    } catch (error) {
        throw new Error('cannot update category')
    }
}



