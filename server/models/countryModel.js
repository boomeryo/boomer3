var mongoose = require('mongoose');


// User Schema
var CountriesSchema = mongoose.Schema({
    country:{type: String, required:true },
    continent:{type: String, required:true, lowerCase: true }
  });

// var countries = module.exports = mongoose.model('countries', CountriesSchema);
var countries = module.exports = mongoose.model('countries2', CountriesSchema);



// //Return the whole collection
// module.exports.returnCollection=function(){
//         return countries.find({},{name:1,_id:0});
//     };

//Return the whole collection
module.exports.returnCollection2 = function () {
    return countries.find({},{country:1,_id:0,continent:1});
};