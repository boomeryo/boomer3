import Vue from 'vue'
import Router from 'vue-router'
import authService from '@/components/services/authService'
//mainSiteParts
import signUp from '@/components/signUp'
import Login from '@/components/login'
import mainPage from '@/components/mainPage'
import market from '@/components/market'
import forms from '@/components/forms'
import userDashboard from '@/components/user_control_pannel/dashboard'
import dealPage from '@/components/deals/dealPage'
import helpCenter from '@/components/helpCenter'
import forgotPassword from '@/components/forgotPassword'
import resetPassword from '@/components/resetPassword'
import notFound from '@/components/notFound'
import contactb from '@/components/contactb'
import about from '@/components/about'

//control panel
import profileSettings from '@/components/user_control_pannel/profile'
import dealsCp from '@/components/user_control_pannel/dealsCp'
import inbox from '@/components/user_control_pannel/inbox'
import contacta from '@/components/user_control_pannel/contacta'

//admin
import adminDashboard from '@/components/admin/adminDashboard'
import manageUsers from '@/components/admin/manageUsers'
import manageDeals from '@/components/admin/manageDeals'
import manageCategories from '@/components/admin/manageCategories'
import dashboard from '@/components/admin/dashboard'

//VUEX
import store from '../store'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/about',
      name: 'about',
      component: about
    },
    {
      path: '/signUp',
      name: 'signUp',
      component: signUp
    },
    {
      path: '/forgotPassword',
      name: 'forgotPassword',
      component: forgotPassword
    },
    {
      path:'/',
      name: 'mainpage',
      component: mainPage
    },
    {
      path: '/helpCenter',
      name: 'helpCenter',
      component: helpCenter,
    },
    {
      path: '/forms/:formName/:username/:dealID',
      name: 'forms',
      component: forms,
      props: true,
      //a user will not be able to edit other user deal
      beforeEnter: (to, from, next) => {
        if (to.params.formName === 'editDeal') {
          if (store.getters.getUserName === to.params.username) {
            authUserNavigation(to, from, next)
          }
          else {
            console.log('this is not your deal!')
            next(false)
          }
        } else if (to.params.formName === 'addDeal') {
          console.log(store.getters.getUserName)
            authUserNavigation(to, from, next)
        }
      }
    },
    {
      path:'/cp',
      component: userDashboard,
      beforeEnter: (to, from, next) => {
        authUserNavigation(to, from, next)
      },
      children: [
        { path: 'profile', component: profileSettings, name:'profileSettings'},
        { path: 'deals', component: dealsCp, name: 'myDeals' },
        { path: 'inbox', component: inbox, name: 'inbox' },
        { path: 'contact', component: contacta, name: 'contact'},
        // { path: '/helpcenter', component:  },
      ]
    },
    {
      path:'/deals/:dealID',
      name: 'dealPage',
      component: dealPage,
      props: true,
      beforeEnter: (to, from, next) => {
        authUserNavigation(to, from, next)
      }
    },
  {
    path: '/market',
    name: 'market',
    component: market,
    beforeEnter: (to, from, next) => {
      authUserNavigation(to, from, next)
    }
    },
    {
      path: '/reset/',
      name: 'resetPassword',
      component: resetPassword,
      props:true
    },
    {
      path: '/contact',
      name: 'contactb',
      component: contactb
    },
    {
      path: '/404',
      name:'notFound',
      component: notFound
    },
    {
      path: '*',
      redirect: notFound
    },
    {
      path: '/admin',
      component: adminDashboard,
      children: [
        { path: 'dashboard', component: dashboard, name: 'dashboard' },
        { path: 'manageUsers', component: manageUsers, name: 'manageUsers' },
        { path: '/manageDeals', component: manageDeals, name: 'manageDeals' },
        { path: '/manageCategories', component: manageCategories, name: 'manageCategories' },
      ],
      beforeEnter: async (to, from, next) => {
        let userIsAdmin = await authService.methods.userIsAdmin()
        if (userIsAdmin === true && store.getters.getUserName!=null && store.getters.getUserStatus === true) {
          console.log('user Is Admin')
          return next();
        }
        next({ name: 'Login' });
      }
    }
  ]
})



//*************************FUNCTIONS RELATED TO NAVIGATION!***********************/
async function authUserNavigation(to, from, next) {
  //check if user is logged in
  if (store.getters.getUserName != null && store.getters.getUserStatus === true) {
  next();
  } else {
    console.log('user is not logged in!')
    next({ path: '/login' });
  }
}



// router.beforeEach((to, from, next) => {
//   // console.log('global beforeEach')
//   // console.log(store.getters.getUserName)
//   // if (store.getters.getUsername && store.isLoggedIn === true) {
//   //   next();
//   // } else {
//   //   console.log('you can go there without being online')
//   //   next({path:'/login'});
//   // }
//   })

