// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
import App from './App'
import store from'./store'
import httpService from './components/services/httpService.vue'
import authService from './components/services/authService.vue'
import notificationService from './components/services/notificationService.vue'
import navBar from './components/navBar.vue'
import dealsArea from './components/dealsArea.vue'
import searchFilterMenu from './components/searchFilterMenu.vue'
import addDeal from './components/forms/addDeal.vue'
import editDeal from './components/forms/editDeal.vue'
import quickLookModal from './components/modals/quickLookModal.vue'
import infiniteScroll from 'vue-infinite-scroll'
import dealPage from './components/deals/dealPage.vue'
import dealCard from './components/deals/dealCard.vue'
import signUp from './components/signUp.vue'
import router from './router'
import VueCarousel from 'vue-carousel';
import countryField from './components/countryField.vue'
import categories from './components/categories.vue'
import footerb from './components/footerb'

// require styles
import 'swiper/dist/css/swiper.css'
// user control pannel components
import profileSettings from './components/user_control_pannel/profile'
import dealsCp from './components/user_control_pannel/dealsCp'
import inbox from './components/user_control_pannel/inbox'
import dashboard from './components/user_control_pannel/dashboard'
import resetPassword from './components/resetPassword'
import contacta from './components/user_control_pannel/contacta'
import contactb from './components/contactb'
//chat
import VueChatScroll from 'vue-chat-scroll'
import chatsList from './components/templates/chatsList.vue'
//sockets
import VueSocketio from 'vue-socket.io-extended';
import io from 'socket.io-client';
// Import Vue and vue2-collapse
import VueCollapse from 'vue2-collapse'
//accordion
import accordionItem from './components/accordion/accordionItem.vue'
//font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faUpload, faFrown,faHeart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faCoffee)
library.add(faUpload)
library.add(faFrown)
library.add(faHeart)



Vue.component('font-awesome-icon', FontAwesomeIcon)

//components
Vue.component('categories',categories)
Vue.component('countryField',countryField)
Vue.component('accordion-item', accordionItem);
Vue.component('httpService',httpService);
Vue.component('authService', authService);
Vue.component('notificationService',notificationService);
Vue.component('searchFilterMenu',searchFilterMenu);
Vue.component('dealsArea',dealsArea);
Vue.component('dealPage', dealPage);
Vue.component('dealCard', dealCard);
Vue.component('addDeal', addDeal);
Vue.component('editDeal',editDeal);
Vue.component('navBar',navBar);
Vue.component('profileSettings', profileSettings);
Vue.component('dealsUarea', dealsCp);
Vue.component('inbox',inbox);
Vue.component('contact', contacta);
Vue.component('contactb',contactb);

Vue.component('dashboard', dashboard);
Vue.component('signUp', signUp);
Vue.component('footerb', footerb);

//chat
Vue.component('chatsList', chatsList)

Vue.component('resetPassword',resetPassword);

//Modals
Vue.component('quickLookModal',quickLookModal);

// Loading the plugin into the Vue.
Vue.use(VueCollapse)
//modals
Vue.use(Buefy);
Vue.use(infiniteScroll);
Vue.use(authService);
Vue.use(VueCarousel);

// sockets chat
Vue.use(VueSocketio, io('https://boomerb2b.com',{path:'/socket'}));
Vue.use(VueChatScroll)


Vue.config.productionTip = false
Vue.prototype.$eventHub =new Vue();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

