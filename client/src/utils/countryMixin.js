import httpService from '../components/services/httpService.vue'
export const countryMixin = {
  data(){
    return {
      countries: [],
      country: '',
      openOnFocus: true,

    }
  }, methods: {
    async getCountries(){
      try {
        let countries = await httpService.methods.httpReq("api/countrylist", {}, "get");
        this.countries = countries.data;
        console.log('countries fetched from server')
      } catch (e) {
        console.error("cannot get country list from server");
      }
    }
  },
  beforeMount() {
    this.getCountries();
  },
  computed: {
    filteredCountries: function () {
      return this.countries.filter(option => {
          if (option != null && this.country != null) {
              return (
                  option.country
                  .toString()
                  .toLowerCase()
                  .indexOf(this.country.toLowerCase()) >= 0
              );
          }
      });
    }
  }
}
