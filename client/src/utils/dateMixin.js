export const dateMixin = {
    methods: {
            //format the date like: "24 May 2018"
            formatDate: function (dateS) {
                let date = new Date(dateS);
                let monthNames = [
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
                ];

                let day = date.getDate();
                let monthIndex = date.getMonth();
                let year = date.getFullYear();

                return day + " " + monthNames[monthIndex] + " " + year;
        },
        formatTime: function (dateObj) {
            //format time like: hh:mm
            let date = new Date(dateObj)
            let hours = date.getHours();
            let minutes = (date.getMinutes()<10?'0':'') + date.getMinutes()
            return hours+":"+minutes
        }
    }
    
}
