export const dealsMixin = {
  data() {
    return {
    }
  },
  methods: {
    setCountryAndContinent: function(e) {
      if (e.country != null) {
        console.log(e);
        this.deal.continent = e.continent;
        this.deal.country = e.country;
      } else {
        console.log("is null!");
        this.deal.country = null;
        this.deal.continent = null;
      }
    },
    setCountryAndContinentSignUp: function(e) {
      if (e.country != null) {
        console.log(e);
        this.data.continent = e.continent;
        this.data.country = e.country;
      } else {
        console.log("is null!");
        this.data.country = null;
        this.data.continent = null;
      }
    },
    setCountryAndContinentProfile: function(e) {
      if (e.country != null) {
        console.log(e);
        this.user.continent = e.continent;
        this.user.country = e.country;
      } else {
        console.log("is null!");
        this.user.country = null;
        this.user.continent = null;
      }
    },
    setCountryAndContinentMarket: function (e) {
      if (e.country != null) {
        console.log(e);
        this.country = e.country;
      } else {
        console.log("is null!");
        this.country = null;
      }
    }
  }
}
