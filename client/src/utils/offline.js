import notificationService from '../components/services/notificationService.vue'
export const offline = {
  data(){
    return {
    }
  }, methods: {
    offlineMessage: function () {
      notificationService.methods.openSnackBar('We detected intrenet issue? are you sure you are connected?', 'is-success', 'is-top-right')
    }
  },
}
