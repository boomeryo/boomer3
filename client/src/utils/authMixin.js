import httpService from '../components/services/httpService.vue'
import store from '../store'
export const authMixin = {
  data(){
    return {
    }
  }, methods: {
    getCountries:async function(){
      try {
        let userIsBlocked = await httpService.methods.httpReq("users/blocked", {username:store.getters.getUserName}, "get");
          if (userIsBlocked === true) {
            this.$router.push({name:'Login'})
        }
      } catch (e) {
          console.log(e)
      }
    }
  },
  beforeCreated() {
    this.getCountries();
  },
}
