import Vue from 'vue';
import Vuex from 'vuex';
import router from './router/index'
import createPersistedState from "vuex-persistedstate";
import httpService from '../src/components/services/httpService.vue'
import * as Cookie from 'js-cookie'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isUserLoggedIn: false,
    whoIsLoggedIn: null,
    token: 'null',
    isBlocked: false,
    filters: null,
    isAdmin:false
  },
  getters: {
    getUserStatus(state) {
      return state.isUserLoggedIn
    },
    getUserName(state) {
      return state.whoIsLoggedIn
    },
    getToken(state) {
      return state.token
    },
    getBlockedStatus(state) {
      return state.isBlocked
    },
    getFilters(state){
      return state.filters
    },
    getIsUserAdmin(state) {
      return state.isAdmin
    }
  },
  mutations: {
    LoggedStatus(state, status) {
      state.isUserLoggedIn = status;
    },
    setLoggedUserName(state, username) {
      state.whoIsLoggedIn = username;
    },
    setToken(state, token) {
      state.token = token
    },
    setBlockedStatus(state, status) {
      state.isBlocked=status
    },
    setFilters(state, filter) {
      state.filters=filter
    },
    setIsAdmin(state, val) {
      state.isAdmin=val
    }
  },
  actions: {
    checkIfBlocked: async ({ commit,state }) => {
      let isBlocked = await httpService.methods.httpReq('users/isBlocked', { username: state.whoIsLoggedIn  }, 'post')
      console.log(isBlocked)
      if (isBlocked.data === true) {
        console.log('isBlocked')
        //user is blocked
        commit('setBlockedStatus', isBlocked)
        commit('LoggedStatus', false)
        commit('setLoggedUserName', null)
        commit('setToken', 'null')
        router.push({name:'Login'})
      }
    },
    clearFilters: ({ commit}) => {
      commit('setFilters',null)
    },
    applyFilters: ({ commit },filters) => {
      commit('setFilters',filters)
    },
    userLoggedIn: ({ commit }, data) => {
      if (data.token) {
        console.log('with new token')
        commit('setToken',data.token)
      }
      commit('LoggedStatus', data.isLoggedIn)
      commit('setLoggedUserName', data.username)
      commit('setIsAdmin',data.isAdmin)
      console.log('userLoggedIn!')

    },
    userLoggedOut: ({ commit }) => {
      //will reset data once the user
      //is getting 403 statuses 
      commit('LoggedStatus', false)
      commit('setLoggedUserName', null)
      commit('setToken', null)
      commit('setIsAdmin',false)
      console.log('userLoggedOut!')
    },
  },
  plugins: [createPersistedState({
    paths: ['whoIsLoggedIn', 'isUserLoggedIn', 'token','isBlocked','filters','isAdmin'],
    getState: (key) => Cookie.getJSON(key),
    setState: (key, state) => Cookie.set(key, state, {
      expires: 10
    })

  })],

});
